﻿## Kārlis.id.lv

---
## 24.02.2022.
Satura un dizaina uzlabojumi

1. Veikti teksta satura uzlabojumi.
2. Nomainīti daži attēli.
3. Gada skaitlis lapas beigās vairs nav statiska vērtība. Tagad tiek ielasīts caur javascript.
---

---
## 15.03.2019.
Optimizācijas un dizaina izmaiņas

1. Sākuma ekrāna iemaņu saraksta animācijai samazināts periods, pēc kura nomainas teksts.
2. Veikti teksta satura uzlabojumi.
3. Noformējuma pielāgojumi failam **style.css**.
4. Optimizācijai samazināts **jpg** un **png** bilžu izmērs.
---

## 26.11.2018.
Optimizācijas izmaiņas

1. Bildes tika samazinātas līdz pārāk mazam izmēram, kā rezultātā tika novērota pikseļošanās. Bildes saglabātas ar augstumu, kas ir divreiz lielāks par attēloto augstumu.
2. Mapē **images** esošais saturs sakārtots pa apakšmapēm.
3. Poga ar ID **toggle_layered_image** vairs neizmanto **Unicode_IEC_symbol** fontu, tagad tiek izmantota bilde. Fonts vairs netiek izmantots, uzlabots ielādes laiks.
4. Labojumi bilžu ceļos **html** un **css** failos.
5. Noformējuma pielāgojumi failam **style.css**.
---

## 26.11.2018.
Bāzes versija mājaslapai

1. Izveidots index.html
2. Izveidots robots.txt
3. Izveidots sitemap.xml
4. Izveidota **js** mape, kas uzglabā javascript failus.
5. Izveidota **css** mape, kas uzglabā css failus.
6. Izveidota **images** mape, kas uzglabā bildes un animācijas.
7. Izveidota **fonts** mape, kas uzglabā fontus.
8. Optimizācijai minimizēti **css** un **js** faili.
9. Optimizācijai samazināts **jpg** un **png** bilžu izmērs.
---